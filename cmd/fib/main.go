package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"github.com/go-logr/zapr"
	"gitlab.com/gitlab-org/opstrace/fibdemo/pkg/fibber"
	"gitlab.com/gitlab-org/opstrace/fibdemo/pkg/instrumentation"
	"go.opentelemetry.io/otel"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	metricsAddr string
	serviceAddr string
	logLevel    string

	otlpEndpoint        string
	otlpCACertificate   string
	otlpTokenSecretFile string
)

func parseCmdline() {
	flag.StringVar(&metricsAddr,
		"metrics-bind-address", ":8081", "The address the metric endpoint binds to.")
	flag.StringVar(&serviceAddr,
		"service-bind-address", ":8080", "The address the service endpoint binds to.")

	flag.Parse()
}

func setupLogger() (*zap.SugaredLogger, func()) {
	encoderConfig := zap.NewDevelopmentEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder

	loggerCfg := zap.Config{
		Level:             zap.NewAtomicLevelAt(zapcore.DebugLevel),
		Development:       true,
		DisableCaller:     false,
		DisableStacktrace: true,
		Encoding:          "console",
		EncoderConfig:     encoderConfig,
		OutputPaths:       []string{"stderr"},
		ErrorOutputPaths:  []string{"stderr"},
	}
	logger, err := loggerCfg.Build()
	if err != nil {
		panic(fmt.Errorf("building logger: %w", err).Error())
	}

	syncF := func() {
		if err := logger.Sync(); err != nil {
			fmt.Printf("failed to flush logger: %v\n", err)
		}
	}

	otel.SetLogger(zapr.NewLogger(logger))

	return logger.Sugar(), syncF
}

func main() {
	os.Exit(body())
}

func body() int {
	var err error

	logger, syncF := setupLogger()
	defer syncF()

	parseCmdline()

	ctx, cancelFunc := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer cancelFunc()

	// Initialize metrics:
	metricsRegistry, routerMetrics := instrumentation.ConstructPromMeterTooling()

	// Initialize gin router itself
	gin.SetMode(gin.DebugMode)
	routerMain := gin.New()
	//  set ContextWithFallback=true so that gin.Context() can be directly used
	//  by otel to fetch current span
	routerMain.ContextWithFallback = true
	routerMain.Use(
		ginzap.GinzapWithConfig(
			logger.Desugar(),
			&ginzap.Config{
				TimeFormat: time.RFC3339Nano,
				UTC:        true,
				SkipPaths:  []string{"/readyz"},
			},
		),
	)
	routerMain.Use(ginzap.RecoveryWithZap(logger.Desugar(), true))

	// Route metrics:
	routerMain.Use(instrumentation.RouteMetricsPrometheus(metricsRegistry))

	fibr := fibber.New(metricsRegistry)

	v1 := routerMain.Group("/v1")
	{
		api := v1.Group("/api/")
		{
			api.GET("/fibonacci", fibHandler(logger, fibr))
		}
	}

	metricServer := &http.Server{
		Addr:              metricsAddr,
		Handler:           routerMetrics,
		ReadHeaderTimeout: time.Second * 3,
	}

	mainServer := &http.Server{
		Addr:              serviceAddr,
		Handler:           routerMain,
		ReadHeaderTimeout: time.Second * 3,
	}

	err = serveWithGracefulShutdown(ctx, logger, mainServer, metricServer)
	if err != nil {
		logger.Errorf("shutting down http servers: %w", err)
		return 1
	}

	logger.Info("server shutdown is complete")
	return 0

}

type queryParams struct {
	FibN int `json:"n" form:"n" binding:"required,numeric,min=1"`
}

func fibHandler(logger *zap.SugaredLogger, fibr *fibber.FibberT) gin.HandlerFunc {
	return func(ginCtx *gin.Context) {
		// bind query params
		var qp queryParams
		if err := ginCtx.ShouldBindQuery(&qp); err != nil {
			ginCtx.AbortWithError(
				http.StatusBadRequest, fmt.Errorf("path parameters validation failed: %w", err),
			)
			return
		}

		logger.Infow("handling request", zap.Int("n", qp.FibN))
		res := fibr.Lookup(ginCtx, qp.FibN)

		ginCtx.String(http.StatusOK, res)
	}
}

func serveWithGracefulShutdown(ctx context.Context, logger *zap.SugaredLogger, srvs ...*http.Server) error {
	var errs []error

	for _, srv := range srvs {
		srv := srv
		go func() {
			// service connections
			if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
				logger.Errorw("failed to start server", zap.Error(err))
			}
		}()
	}

	// Wait for interrupt signal to gracefully shutdown the server
	<-ctx.Done()

	logger.Info("shutting down gracefully")

	// Allow 10 seconds to flush existing connections
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	wg := sync.WaitGroup{}
	wg.Add(len(srvs))
	for _, srv := range srvs {
		srv := srv
		go func() {
			if err := srv.Shutdown(ctx); err != nil {
				errs = append(errs, fmt.Errorf("server shutdown error: %w", err))
			}
			wg.Done()
		}()
	}
	wg.Wait()

	logger.Info("http servers have exited")

	if len(errs) > 0 {
		return errors.Join(errs...)
	}

	return nil
}
